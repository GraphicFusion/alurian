<?php
/**
 * Ava child theme functions
 *
 * @package Ava
 * @author  Rich Tabor <hello@themebeans.com>
 * @license http://www.gnu.org/licenses/gpl-3.0.html GNU Public License
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 */
function add_file_types_to_uploads($file_types){ 
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes ); 
	return $file_types;
} 
add_action('upload_mimes', 'add_file_types_to_uploads');
