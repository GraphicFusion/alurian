<?php
/**
 * Updater Config
 *
 * @package     Ava
 * @link        https://themebeans.com/themes/ava
 * @author      Rich Tabor of ThemeBeans <hello@themebeans.com>
 * @copyright   Copyright (c) 2018, ThemeBeans of Inventionn LLC
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU Public License
 */

$updater = new ThemeBeans_License(

	$config = array(
		'remote_api_url'    => 'https://themebeans.com',
		'item_name'         => esc_attr( themebeans_get_theme( false ) ),
		'theme_slug'        => esc_attr( themebeans_get_theme( true ) ),
		'version'           => esc_attr( wp_get_theme( get_template() )->get( 'Version' ) ),
		'author'            => 'ThemeBeans',
		'download_id'       => '130853',
		'renew_url'	    => '',
		'beta'	            => false,
	),
	$strings = array(
		'enter-key'                 => wp_kses( __( 'Add your license key to get theme updates without leaving your dashboard and to access support. Locate your license key in your ThemeBeans account dashboard and on your purchase receipt.<br><br>Enter your license key below:', 'ava' ), array( 'br' => array() ) ),
		'license-key'               => esc_html__( 'License Key', 'ava' ),
		'license-action'            => esc_html__( 'License Action', 'ava' ),
		'enter-license-label'       => esc_html__( '', 'ava' ),
		'deactivate-license'        => esc_html__( 'Deactivate License', 'ava' ),
		'reload-button'             => esc_html__( 'Reload', 'ava' ),
		'activate-license'          => esc_html__( 'Activate License', 'ava' ),
		'reactivate-button'         => esc_html__( 'Reactivate License', 'ava' ),
		'status-unknown'            => esc_html__( 'License status is unknown.', 'ava' ),
		'renew'                     => esc_html__( 'renew your theme license.', 'ava' ),
		'renew-after'               => esc_html__( 'After completing the renewal, click the Reload button below.', 'ava' ),
		'renew-button'              => esc_html__( 'Renew your License &rarr;', 'ava' ),
		'unlimited'                 => esc_html__( 'unlimited', 'ava' ),
		'license-key-is-active%s'   => esc_html__( 'Seamless theme updates and support has been enabled for %s. You will receive a notice in your dashboard when an update is available.', 'ava' ),
		'expires%s'                 => esc_html__( '', 'ava' ),
		'%1$s/%2$-sites'            => esc_html__( 'You have %1$s / %2$s sites activated.', 'ava' ),
		'license-key-expired-%s'    => esc_html__( 'Your license key expired on %s. In order to access support and seamless updates, you need to', 'ava' ),
		'license-key-expired'       => esc_html__( 'License key has expired.', 'ava' ),
		'license-keys-do-not-match' => esc_html__( 'This is not a valid license key. Please try again.', 'ava' ),
		'license-is-inactive'       => esc_html__( 'License is inactive.', 'ava' ),
		'license-key-is-disabled'   => esc_html__( 'License key is disabled.', 'ava' ),
		'site-is-inactive'          => esc_html__( 'Site is inactive.', 'ava' ),
		'license-status-unknown'    => esc_html__( 'License status is unknown.', 'ava' ),
		'update-notice'             => esc_html__( 'Updating this theme will lose any customizations you have made. "Cancel" to stop, "OK" to update.', 'ava' ),
		'update-available'          => __( '<strong>%1$s %2$s</strong> is available. <a href="%3$s" title="%4s" target="blank">Check out what\'s new</a> or <a href="%5$s" %6$s>update now</a>', 'ava' ),
	)
);
