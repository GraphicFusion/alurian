<?php
/**
 * Title Customizer Control
 *
 * @see https://developer.wordpress.org/reference/classes/wp_customize_control/
 *
 * @package     Ava
 * @link        https://themebeans.com/themes/ava
 * @author      Rich Tabor of ThemeBeans <hello@themebeans.com>
 * @copyright   Copyright (c) 2018, ThemeBeans of Inventionn LLC
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU Public License
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Exit if WP_Customize_Control does not exsist.
if ( ! class_exists( 'WP_Customize_Control' ) ) {
	return null;
}

/**
 * This class is for the title control in the Customizer.
 *
 * @access  public
 */
class ThemeBeans_License_Control extends WP_Customize_Control {

	/**
	 * Set the control type.
	 *
	 * @var $type Customizer type
	 */
	public $type = 'themebeans-license';

	/**
	 * Enqueue neccessary custom control scripts.
	 * Localization occurs in the Login_Designer_Customizer_Scripts() class (based on SCRIPT_DEBUG).
	 */
	public function enqueue() {
		wp_enqueue_style( 'themebeans-license-control', get_theme_file_uri( '/inc/admin/controls/license/license.css' ), false, '1.5.6', 'all' );
		wp_enqueue_script( 'themebeans-license-control', get_theme_file_uri( '/inc/admin/controls/license/license.js' ), array( 'jquery' ), '1.5.6', true );

		// Localization.
		$localize = array(
			'nonce'   => array( 'activate' => wp_create_nonce( 'themebeans-activate-license' ), 'deactivate' => wp_create_nonce( 'themebeans-deactivate-license' ) ),
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		);

		wp_localize_script( 'themebeans-license-control', 'themebeans_license_control', $localize );
	}

	/**
	 * Render the content.
	 *
	 * @see https://developer.wordpress.org/reference/classes/wp_customize_control/render_content/
	 */
	public function render_content() {

		$customizer 		= new ThemeBeans_License();
		$key 			= $customizer->key();
		$status 		= $customizer->status();
		$expiration 		= $customizer->expiration();
		$site_count 		= $customizer->site_count();
		$activations_left 	= $customizer->activations_left();
		$is_valid 		= $customizer->is_valid_license();
		$visibility 		= ( true === $is_valid ) ? 'is-valid' : 'is-not-valid';

		// Array of allowed HTML.
		$allowed_html_array = array(
			'a' => array(
				'href'   => array(),
				'target' => array(),
			),
		);

		if ( isset( $this->label ) ) {
			echo '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';
		}

		if ( ! empty( $this->description ) ) {
			echo '<span class="customize-control-description">' . wp_kses( $this->description, $allowed_html_array ) . '</span>';
		}
		?>

		<form id="theme-license-form" name="license-form">
			<input id="theme-license-key" class="license" name="theme-license-key" spellcheck="false" type="text" <?php $this->input_attrs(); ?> value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
			<input type="submit" name="themebeans-license" id="themebeans-activate-license" value="<?php echo esc_attr__( 'Activate', 'ava' ); ?>" class="button-secondary button <?php echo esc_attr( $visibility ); ?>">
			<input type="submit" name="themebeans-deactivate-license" id="themebeans-deactivate-license" value="<?php echo esc_attr__( 'Deactivate', 'ava' ); ?>" class="button-secondary button <?php echo esc_attr( $visibility ); ?>">
			<div class="spinner"></div>
		</form>

		<div id="theme-license-error"></div>
		<ul id="theme-license-info" class="<?php echo esc_attr( $visibility ); ?>">
			<li><strong><?php echo esc_html__( 'Status:', 'ava' ); ?></strong> <span id="theme-license-status"><?php echo esc_html( $status ); ?></span></li>
		<ul>

		<?php
	}
}