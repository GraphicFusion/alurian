<?php
/**
 * Range Customizer Control
 *
 * @see https://developer.wordpress.org/reference/classes/wp_customize_control/
 *
 * @package     Ava
 * @link        https://themebeans.com/themes/ava
 * @author      Rich Tabor of ThemeBeans <hello@themebeans.com>
 * @copyright   Copyright (c) 2018, ThemeBeans of Inventionn LLC
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU Public License
 */

/**
 * Custom Range Control
 */
class ThemeBeans_Range_Control extends WP_Customize_Control {
	/**
	 * Set the control type.
	 *
	 * @var $type Customizer type
	 */
	public $type = 'custom-range';

	/**
	 * Get the control default.
	 *
	 * @var $type Customizer type option
	 */
	public $default = 'default';

	/**
	 * Enqueue scripts and styles.
	 */
	public function enqueue() {
		wp_enqueue_style( 'themebeans-range-control-styles', get_theme_file_uri( '/inc/admin/controls/range/range.css' ), false, '1.5.6', 'all' );
		wp_enqueue_script( 'themebeans-range-control-scripts', get_theme_file_uri( '/inc/admin/controls/range/range.js' ), array( 'jquery' ), '1.5.6', true );
	}

	/**
	 * Render the content.
	 *
	 * @see https://developer.wordpress.org/reference/classes/wp_customize_control/render_content/
	 */
	public function render_content() {

		if ( ! empty( $this->label ) ) : ?>
			<label><span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span></label>
		<?php endif; ?>

		<div class="value">
			<span><?php echo esc_attr( $this->value() ); ?></span>
			<input class="track-input" data-default-value="<?php echo esc_html( $this->default ); ?>" type="number"<?php $this->input_attrs(); ?> value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
			<em><?php echo esc_html( $this->description ); ?></em>
		</div>

		<input class="track" data-default-value="<?php echo esc_attr( $this->default ); ?>" data-input-type="range" type="range"<?php $this->input_attrs(); ?> value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />

		<a type="button" value="reset" class="range-reset"></a>

	<?php
	}
}
