<?php
/**
 * Toggle Customizer Control
 *
 * @see https://developer.wordpress.org/reference/classes/wp_customize_control/
 *
 * @package     Ava
 * @link        https://themebeans.com/themes/ava
 * @author      Rich Tabor of ThemeBeans <hello@themebeans.com>
 * @copyright   Copyright (c) 2018, ThemeBeans of Inventionn LLC
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU Public License
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Exit if WP_Customize_Control does not exsist.
if ( ! class_exists( 'WP_Customize_Control' ) ) {
	return null;
}

/**
 * This class is for the toggle control in the Customizer.
 *
 * @access public
 */
class ThemeBeans_Toggle_Control extends WP_Customize_Control {

	/**
	 * Enqueue scripts and styles.
	 */
	public function enqueue() {
		wp_enqueue_style( 'themebeans-toggle-control-styles', get_theme_file_uri( '/inc/admin/controls/toggle/toggle.css' ), false, '1.5.6', 'all' );
		wp_enqueue_script( 'themebeans-toggle-control-scripts', get_theme_file_uri( '/inc/admin/controls/toggle/toggle.js' ), array( 'jquery' ), '1.5.6', true );
	}

	/**
	 * Render the control's content.
	 */
	public function render_content() {
		?>
		<label class="toggle">
			<div class="toggle--wrapper">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<input id="toggle-<?php echo esc_attr( $this->instance_number ); ?>" type="checkbox" class="toggle--input" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); checked( $this->value() ); ?> />
				<label for="toggle-<?php echo esc_attr( $this->instance_number ); ?>" class="toggle--label"></label>
			</div>
			<?php if ( ! empty( $this->description ) ) { ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
			<?php } ?>
		</label>
		<?php
	}
}
