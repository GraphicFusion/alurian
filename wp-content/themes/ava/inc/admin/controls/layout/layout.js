(function ($) {
    "use strict";

    $(document).ready(function ($) {
        $('.layout-switcher').on('click', function (e) {
            e.preventDefault();
            $('.layout-switcher__wrapper').toggleClass( 'open' );

            if ($(this).text() === 'Layout') {
                 $(this).text('Close');
            }
            else {
                $(this).text('Layout');
            }
        });
    });
})(jQuery);