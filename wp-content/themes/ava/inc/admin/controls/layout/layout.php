<?php
/**
 * Layout Customizer Control
 *
 * @see https://developer.wordpress.org/reference/classes/wp_customize_control/
 *
 * @package     Ava
 * @link        https://themebeans.com/themes/ava
 * @author      Rich Tabor of ThemeBeans <hello@themebeans.com>
 * @copyright   Copyright (c) 2018, ThemeBeans of Inventionn LLC
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU Public License
 */

/**
 * Custom Image Layout Control
 */
class ThemeBeans_Layout_Control extends WP_Customize_Control {
	/**
	 * Set the control type
	 *
	 * @var $type Customizer type
	 */
	public $type = 'layout';

	/**
	 * Enqueue scripts and styles.
	 */
	public function enqueue() {
		wp_enqueue_style( 'themebeans-layout-control-styles', get_theme_file_uri( '/inc/admin/controls/layout/layout.css' ), false, '1.5.6', 'all' );
		wp_enqueue_script( 'themebeans-layout-control-scripts', get_theme_file_uri( '/inc/admin/controls/layout/layout.js' ), array( 'jquery' ), '1.5.6', true );
	}

	/**
	 * Render the content.
	 *
	 * @see https://developer.wordpress.org/reference/classes/wp_customize_control/render_content/
	 */
	public function render_content() {

		$name = '_customize-layout-' . $this->id;

		if ( isset( $this->description ) ) {
			echo '<span class="description customize-control-description">' . wp_kses_post( $this->description ) . '</span>';
		} ?>

		<button id="layout-switcher" class="button layout-switcher"><?php esc_html_e( 'Layout', 'ava' ); ?></button>

		<div class="layout-switcher__wrapper">
			<?php foreach ( $this->choices as $value => $label ) { ?>

			   <input id="<?php echo esc_attr( $name ); ?>_<?php echo esc_attr( $value ); ?>" class="layout" type="radio" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> />
					<label for="<?php echo esc_attr( $name ); ?>_<?php echo esc_attr( $value ); ?>">
					<div class="intrinsic">
						<div class="layout-screenshot" style="background-image: url(<?php echo esc_html( $this->choices[ $value ] ); ?>);"></div>
					</div>
			   </label>

			<?php } ?>
		</div>
		<?php
	}
}
