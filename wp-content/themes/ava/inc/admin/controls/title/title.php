<?php
/**
 * Title Customizer Control
 *
 * @see https://developer.wordpress.org/reference/classes/wp_customize_control/
 *
 * @package     Ava
 * @link        https://themebeans.com/themes/ava
 * @author      Rich Tabor of ThemeBeans <hello@themebeans.com>
 * @copyright   Copyright (c) 2018, ThemeBeans of Inventionn LLC
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU Public License
 */

/**
 * Custom Title Control
 */
class ThemeBeans_Title_Control extends WP_Customize_Control {
	/**
	 * Set the control type.
	 *
	 * @var $type Customizer type
	 */
	public $type = 'custom_title';

	/**
	 * Enqueue scripts and styles.
	 */
	public function enqueue() {
		wp_enqueue_style( 'themebeans-title-control-styles', get_theme_file_uri( '/inc/admin/controls/title/title.css' ), false, '1.5.6', 'all' );
	}

	/**
	 * Render the content.
	 *
	 * @see https://developer.wordpress.org/reference/classes/wp_customize_control/render_content/
	 */
	public function render_content() {

		if ( isset( $this->label ) ) {
			echo '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';
		}

		if ( ! empty( $this->description ) ) {
			echo '<div class="customize-control-tooltip-wrapper"><span class="customize-control-tooltip hint hint--top" data-hint="' . esc_html( $this->description ) . '"><span class="customize-control-tooltip-icon"></span></span></div>';
		}

	}
}
