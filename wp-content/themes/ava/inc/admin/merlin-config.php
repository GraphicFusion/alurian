<?php
/**
 * Merlin WP Configuration file.
 *
 * @package     Ava
 * @link        https://themebeans.com/themes/ava
 * @author      Rich Tabor of ThemeBeans <hello@themebeans.com>
 * @copyright   Copyright (c) 2018, ThemeBeans of Inventionn LLC
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU Public License
 */

if ( ! class_exists( 'Merlin' ) ) {
	return;
}

/**
 * Set directory locations, text strings, and other settings for Merlin WP.
 */
$wizard = new Merlin(
	// Configure Merlin with custom settings.
	$config = array(
		'directory' 				=> 'inc/admin/', 									// Location where the 'merlin' directory is placed.
		'demo_directory' 			=> 'inc/demo/',  									// Location where the theme demo files exist.
		'merlin_url' 				=> 'merlin',     									// Customize the page URL where Merlin WP loads.
		'child_action_btn_url' 			=> 'https://codex.wordpress.org/Child_Themes', 						// The URL for the 'child-action-link'.
		'help_mode' 				=> false, 										// Set to true to turn on the little wizard helper.
		'dev_mode' 				=> false,
		'branding' 				=> false, 										// Set to false to remove Merlin WP's branding.
	),
	// Text strings.
	$strings = array(
		'admin-menu' 				=> esc_html__( 'Theme Setup' , 'ava' ),
		'title%s%s%s%s' 			=> esc_html__( '%s%s Themes &lsaquo; Theme Setup: %s%s' , 'ava' ),

		'return-to-dashboard' 			=> esc_html__( 'Return to the dashboard' , 'ava' ),

		'btn-skip' 				=> esc_html__( 'Skip' , 'ava' ),
		'btn-next' 				=> esc_html__( 'Next' , 'ava' ),
		'btn-start' 				=> esc_html__( 'Start' , 'ava' ),
		'btn-no' 				=> esc_html__( 'Cancel' , 'ava' ),
		'btn-plugins-install' 			=> esc_html__( 'Install' , 'ava' ),
		'btn-child-install' 			=> esc_html__( 'Install' , 'ava' ),
		'btn-content-install' 			=> esc_html__( 'Install' , 'ava' ),
		'btn-import' 				=> esc_html__( 'Import' , 'ava' ),
		'btn-license-activate' 			=> esc_html__( 'Activate' , 'ava' ),

		'welcome-header%s' 			=> esc_html__( 'Welcome to %s' , 'ava' ),
		'welcome-header-success%s' 		=> esc_html__( 'Hi. Welcome back' , 'ava' ),
		'welcome%s' 				=> esc_html__( 'This wizard will set up your theme, install plugins, and import content. It is optional & should take only a few minutes.' , 'ava' ),
		'welcome-success%s' 			=> esc_html__( 'You may have already run this theme setup wizard. If you would like to proceed anyway, click on the "Start" button below.' , 'ava' ),

		'child-header' 				=> esc_html__( 'Install Child Theme' , 'ava' ),
		'child-header-success' 			=> esc_html__( 'You\'re good to go!' , 'ava' ),
		'child' 				=> esc_html__( 'Let\'s build & activate a child theme so you may easily make theme changes.' , 'ava' ),
		'child-success%s' 			=> esc_html__( 'Your child theme has already been installed and is now activated, if it wasn\'t already.' , 'ava' ),
		'child-action-link' 			=> esc_html__( 'Learn about child themes' , 'ava' ),
		'child-json-success%s' 			=> esc_html__( 'Awesome. Your child theme has already been installed and is now activated.' , 'ava' ),
		'child-json-already%s' 			=> esc_html__( 'Awesome. Your child theme has been created and is now activated.' , 'ava' ),

		'plugins-header' 			=> esc_html__( 'Install Plugins' , 'ava' ),
		'plugins-header-success' 		=> esc_html__( 'You\'re up to speed!' , 'ava' ),
		'plugins' 				=> esc_html__( 'Let\'s install some essential WordPress plugins to get your site up to speed.' , 'ava' ),
		'plugins-success%s' 			=> esc_html__( 'The required WordPress plugins are all installed and up to date. Press "Next" to continue the setup wizard.' , 'ava' ),
		'plugins-action-link' 			=> esc_html__( 'Advanced' , 'ava' ),

		'import-header' 			=> esc_html__( 'Import Content' , 'ava' ),
		'import' 				=> esc_html__( 'Let\'s import content to your website, to help you get familiar with the theme.' , 'ava' ),
		'import-action-link' 			=> esc_html__( 'Advanced' , 'ava' ),

		'license-header%s' 			=> esc_html__( 'Activate %s' , 'ava' ),
		'license' 				=> esc_html__( 'Add your license key to activate one-click updates and theme support.' , 'ava' ),
		'license-action-link' 			=> esc_html__( 'More info' , 'ava' ),

		'license-link-1'            		=> wp_kses( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://wordpress.org/support/', esc_html__( 'Explore WordPress', 'ava' ) ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ),
		'license-link-2'            		=> wp_kses( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://themebeans.com/contact/', esc_html__( 'Get Theme Support', 'ava' ) ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ),
		'license-link-3'           		=> wp_kses( sprintf( '<a href="'.admin_url( 'customize.php' ).'" target="_blank">%s</a>', esc_html__( 'Start Customizing', 'ava' ) ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ),

		'ready-header' 				=> esc_html__( 'All done. Have fun!' , 'ava' ),
		'ready%s' 				=> esc_html__( 'Your theme has been all set up. Enjoy your new theme by %s.' , 'ava' ),
		'ready-action-link' 			=> esc_html__( 'Extras' , 'ava' ),
		'ready-big-button' 			=> esc_html__( 'View your website' , 'ava' ),

		'ready-link-1'            		=> wp_kses( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://wordpress.org/support/', esc_html__( 'Explore WordPress', 'ava' ) ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ),
		'ready-link-2'            		=> wp_kses( sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://themebeans.com/contact/', esc_html__( 'Get Theme Support', 'ava' ) ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ),
		'ready-link-3'           		=> wp_kses( sprintf( '<a href="'.admin_url( 'customize.php' ).'" target="_blank">%s</a>', esc_html__( 'Start Customizing', 'ava' ) ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ),
	)
);

/**
 * Content template for the child theme functions.php file.
 *
 * @link https://gist.github.com/richtabor/688327dd103b1aa826ebae47e99a0fbe
 *
 * @param string $output Generated content.
 * @param string $slug Parent theme slug.
 */
function themebeans_generate_child_functions_php( $output, $slug ) {

	$slug_no_hyphens = strtolower( preg_replace( '#[^a-zA-Z]#', '', $slug ) );

	// Get the parent theme.
	$theme = themebeans_get_theme( true );

	// Check if the parent theme uses an "-" or "." to load the minified stylesheet.
	if ( 'plate' === $theme || 'designer' === $theme || 'emma' === $theme ) {
		$sep = '-';
	} else {
		$sep = '.';
	}

	$output = "
		<?php
		/**
		 * Theme functions and definitions.
		 * This child theme was generated by Merlin WP.
		 *
		 * @link https://developer.wordpress.org/themes/basics/theme-functions/
		 *
		 * If your child theme has more than one .css file (eg. ie.css, style.css, main.css) then
		 * you will have to make sure to maintain all of the parent theme dependencies.
		 *
		 * Make sure you're using the correct handle for loading the parent theme's styles.
		 * Failure to use the proper tag will result in a CSS file needlessly being loaded twice.
		 * This will usually not affect the site appearance, but it's inefficient and extends your page's loading time.
		 *
		 * @link https://codex.wordpress.org/Child_Themes
		 */

		/*
		 * Check if SCRIPT_DEBUG is set to true.
		 * If so, we will load the unminified versions of the main theme stylesheet.
		 * If not, load the compressed and combined version.
		 *
		 * @link https://codex.wordpress.org/WP_DEBUG
		 */
		function {$slug_no_hyphens}_child_enqueue_styles() {

		    if ( SCRIPT_DEBUG ) {
		        wp_enqueue_style( '{$slug}-style' , get_template_directory_uri() . '/style.css' );
		    } else {
		        wp_enqueue_style( '{$slug}-minified-style' , get_template_directory_uri() . '/style{$sep}min.css' );
		    }

		    wp_enqueue_style( '{$slug}-child-style',
		        get_stylesheet_directory_uri() . '/style.css',
		        array( '{$slug}-style' ),
		        wp_get_theme()->get('Version')
		    );
		}

		add_action(  'wp_enqueue_scripts', '{$slug_no_hyphens}_child_enqueue_styles' );\n
	";

	// Let's remove the tabs so that it displays nicely.
	$output = trim( preg_replace( '/\t+/', '', $output ) );

	// Filterable return.
	return $output;
}
add_filter( 'merlin_generate_child_functions_php', 'themebeans_generate_child_functions_php', 10, 2 );

/**
 * Add your widget area to unset the default widgets from.
 * If your theme's first widget area is "sidebar-1", you don't need this.
 *
 * @see https://stackoverflow.com/questions/11757461/how-to-populate-widgets-on-sidebar-on-theme-activation
 *
 * @param  array $widget_areas Arguments for the sidebars_widgets widget areas.
 * @return array of arguments to update the sidebars_widgets option.
 */
function themebeans_merlin_unset_default_widgets_args( $widget_areas ) {

	// Get the parent theme.
	$theme = themebeans_get_theme( true );

	if ( 'plate' === $theme ) {
		$widget_areas = array(
			'sidebar-6' => array(),
		);
	}

	if ( 'designer' === $theme ) {
		$widget_areas = array(
			'theme-sidebar' => array(),
		);
	}

	if ( 'designer' === $theme ) {
		$widget_areas = array(
			'internal-sidebar' => array(),
		);
	}

	return $widget_areas;
}
add_filter( 'merlin_unset_default_widgets_args', 'themebeans_merlin_unset_default_widgets_args' );
