<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 * array	$args['rows']			 //  array of rows of lede/content
	 * string	$args['rows'][0]['lede'] //  row title
	 * string	$args['rows'][0]['hidden_content'] //  row hidden content
	 */
	global $args; 
?>
<?php if( $args['content'] || (is_array($args['rows']) && count($args['rows']) > 0 )) : ?>
	<?php if( $args['title'] ) : ?>
		<h3 class="wow fadeInUp"><?php echo $args['title']; ?></h3>
	<?php endif; ?>
	<div class="accordion-wrapper wow fadeInUp">
		<?php if( $args['content'] ) : ?>
			<div class="accordion-content">
				<?php echo $args['content']; ?>
			</div>
		<?php endif; ?>
		<?php if( count($args['rows']) > 0 ) : ?>
			<dl class="accordion">
			<?php if( is_array( $args['rows'] ) ) : ?>
				<?php foreach( $args['rows'] as $row ): ?>
						<dt>
							<a href="#0">
								<?php echo $row['lede']; ?>
								<?php if ($row['hidden_content']) : ?>
									<div class="ava-override">
<svg class="icon icon--plus" aria-hidden="true" role="img"><use xlink:href="#icon-plus"></use></svg>
</div>
								<?php endif; ?>
							</a>
						</dt>
						<dd><?php echo $row['hidden_content']; ?></dd>
				<?php endforeach; ?>
			<?php endif; ?>
			</dl>
		<?php endif; ?>
	</div>
<?php endif; ?>